<?php
require 'database.php';
$stmt = $mysqli->prepare("SELECT floorname,image FROM floorplan ORDER BY id DESC LIMIT 1");
if(!$stmt){
  $txt = $mysqli->error;
  $txt = "\n";
  exit;
}
$stmt->execute();
$stmt->bind_result($floorplan,$image);
$stmt->fetch();
$stmt->close();
echo json_encode(array(
  "floorplan" => $floorplan,
  "image" => $image
));
exit;
?>
