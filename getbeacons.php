<?php
require 'database.php';

$eventarray = array();
class event {
    public $id;
    public $x;
    public $y;
    public $room;
}

$floorplan = $_GET['floorplan'];
//SELECT * FROM hits WHERE timestamp >= '2017-11-15 00:48:55' AND timestamp <= '2017-11-15 00:48:56'
$stmt = $mysqli->prepare("SELECT id, x, y,room FROM beacons WHERE floorplan=?");
$stmt->bind_param('s', $floorplan);
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}
$stmt->execute();

$stmt->bind_result($resultid,$resultx,$resulty,$resultroom);

while($stmt->fetch()){
	$obj = new event();
	$obj->id = htmlentities($resultid);
	$obj->x = htmlentities($resultx);
	$obj->y = htmlentities($resulty);
  $obj->room = htmlentities($resultroom);
	array_push($eventarray,$obj);
}

$stmt->close();
echo json_encode(array(
	"success" => true,
	"results" => $eventarray
));
exit;
?>
