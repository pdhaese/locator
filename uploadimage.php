<?php
session_start();

// Get the filename and make sure it is valid
$floorplan = $_POST['floorplan'];
$filename = basename($_FILES['uploadedfile']['name']);
echo htmlentities($filename);
if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
	echo "Invalid filename";
	exit;
}

$full_path = sprintf("/home/pdhaese/public_html/locator/uploads/%s", $filename);
if( move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $full_path) ){
  require 'database.php';
  $stmt = $mysqli->prepare("insert into floorplan (floorname,image) values (?, ?)");
  $stmt->bind_param('ss', $floorplan,$filename);
  if(!$stmt){
    $txt = $mysqli->error;
    $txt = "\n";
    exit;
  }
  $stmt->execute();
  $stmt->close();
  $_SESSION['origin'] = $full_path;
	header("Location: addFloor.html");
	exit;
}else{
	header("Location: upload_failure.html");
	exit;
}

?>
