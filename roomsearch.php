<?php
require 'database.php';

$eventarray = array();
class event {
    public $id;
    public $time;
    public $distance;
}

$beaconid = $_GET['beaconid'];
$timestamp1 = $_GET["timestamp1"];
$timestamp2 = $_GET["timestamp2"];
//SELECT * FROM hits WHERE timestamp >= '2017-11-15 00:48:55' AND timestamp <= '2017-11-15 00:48:56'
$stmt = $mysqli->prepare("SELECT partid, timestamp, distance FROM hits WHERE timestamp >= ? AND timestamp <= ? AND beaconid=?");
$stmt->bind_param('sss', $timestamp1,$timestamp2,$beaconid);
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}

$stmt->execute();

$stmt->bind_result($resultId,$resultTimestamp,$resultDistance);

while($stmt->fetch()){
	$obj = new event();
	$obj->id = htmlentities($resultId);
	$obj->time = htmlentities($resultTimestamp);
	$obj->distance = htmlentities($resultDistance);
	array_push($eventarray,$obj);
}

$stmt->close();
echo json_encode(array(
	"success" => true,
	"results" => $eventarray
));
exit;
?>
