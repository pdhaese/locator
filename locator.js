
var allids = [];
var alltimes = [];
var alldistances = [];
var iterator = 0;
var floorplanbeacons = [];
var xcoors = [];
var ycoors = [];
var rooms = [];



function searchByUser(){
  var username = document.getElementById("searchuser").value; // Get the info from the form
  document.getElementById("theuser").innerHTML = username;
  var begdate = document.getElementById("searchdatebeg").value;
  var begtime = document.getElementById("searchtimebeg").value;
  var enddate = document.getElementById("searchdateend").value;
  var endtime = document.getElementById("searchtimeend").value;
  var beg = encodeURIComponent(begdate + ' ' + begtime);
  var end = encodeURIComponent(enddate + ' ' + endtime);
  var myurl = "http://ec2-34-210-27-148.us-west-2.compute.amazonaws.com/~pdhaese/locator/retrievepersonlocation.php?partid=" + encodeURIComponent(username) + "&timestamp1=" + beg + "&timestamp2=" + end;
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": myurl,
    //"url": "http://ec2-34-210-27-148.us-west-2.compute.amazonaws.com/~pdhaese/locator/retrievepersonlocation.php?partid=will&timestamp1=2017-11-15%2000%3A48%3A53&timestamp2=2017-11-15%2000%3A48%3A55",
    "method": "GET",
    "headers": {
      "cache-control": "no-cache",
      "postman-token": "c00090e1-573e-6c16-2e98-e3d359cb37be"
    }
  }
  $.ajax(settings).done(function (response) {
    var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
    parseUserData(jsonData.results);
  });
}

function searchByRoom(){
  document.getElementById("roomtablespace").innerHTML='';
  var roomnumber = document.getElementById("searchroom").value; // Get the info from the form
  var roombeacon = '';
  for(v=0;v<rooms.length;v++){
    if(rooms[v] == roomnumber){
      roombeacon = floorplanbeacons[v];
    }
  }
  var begdate = document.getElementById("searchdatebegroom").value;
  var begtime = document.getElementById("searchtimebegroom").value;
  var enddate = document.getElementById("searchdateendroom").value;
  var endtime = document.getElementById("searchtimeendroom").value;
  var beg = encodeURIComponent(begdate + ' ' + begtime);
  var end = encodeURIComponent(enddate + ' ' + endtime);
  var myurl = "http://ec2-34-210-27-148.us-west-2.compute.amazonaws.com/~pdhaese/locator/roomsearch.php?beaconid=" + encodeURIComponent(roombeacon) + "&timestamp1=" + beg + "&timestamp2=" + end;
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": myurl,
    //"url": "http://ec2-34-210-27-148.us-west-2.compute.amazonaws.com/~pdhaese/locator/retrievepersonlocation.php?partid=will&timestamp1=2017-11-15%2000%3A48%3A53&timestamp2=2017-11-15%2000%3A48%3A55",
    "method": "GET",
    "headers": {
      "cache-control": "no-cache",
      "postman-token": "c00090e1-573e-6c16-2e98-e3d359cb37be"
    }
  }
  $.ajax(settings).done(function (response) {
    var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
    if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData
      parseRoomHistoryData(jsonData.results);

    }
  });
}
function parseRoomHistoryData(results){

  console.log('init');
  console.log(results);
//  var array = {"success":true,"results":[{"id":"will","time":"2017-11-15 00:48:53","distance":"2"},{"id":"will","time":"2017-11-15 00:48:54","distance":"2"},{"id":"will","time":"2017-11-15 00:48:54","distance":"2"},{"id":"will","time":"2017-11-15 00:48:54","distance":"2"},{"id":"will","time":"2017-11-15 00:48:55","distance":"2"},{"id":"will","time":"2017-11-15 00:48:55","distance":"0"}]};

      console.log('hello');
    console.log('results');

    var table = document.createElement('table');
    var th1 = document.createElement('th');
    var header1 = document.createTextNode("Employee")
    var th2 = document.createElement('th');
    var header2 = document.createTextNode("Date");
    var th3 = document.createElement('th');
    var header3 = document.createTextNode("Time");

    th1.appendChild(header1);
    th2.appendChild(header2);
    th3.appendChild(header3);
    table.appendChild(th1);
    table.appendChild(th2);
    table.appendChild(th3);
    for(i = 0; i < results.length; i++){
        var distance = results[i].distance;
        if (distance != 0){
          var tr = document.createElement('tr');

          var td1 = document.createElement('td');
          var td2 = document.createElement('td');
          var td3 = document.createElement('td');

          var user = results[i].id;
          var datetime = results[i].time;
          var split = String(datetime).split(" ");
          time = split[0];
          date = split[1];


          var text1 = document.createTextNode(user);
          var text2 = document.createTextNode(time);
          var text3 = document.createTextNode(date);
          td1.appendChild(text1);
          td2.appendChild(text2);
          td3.appendChild(text3);
          tr.appendChild(td1);
          tr.appendChild(td2);
          tr.appendChild(td3);
          table.appendChild(tr);
        }
    }
    document.getElementById('roomtablespace').appendChild(table);

}

function parseUserData(eventsarray){
  iterator = 0;
  allids = [];
  alltimes = [];
  alldistances = [];
  for(r=0;r<eventsarray.length;r++){
    if(eventsarray[r].distance!=0){
      allids.push(eventsarray[r].id);
      alltimes.push(eventsarray[r].time);
      alldistances.push(eventsarray[r].distance);
    }
  }
  // var myids = [];
  // var mytimes = [];
  // var mydistances = [];
  // var timestamp = '';
  // var distance = 3;
  // for(i = 0; i < eventsarray.length; i++){
  //   var checktime = eventsarray[i].time;
  //   var checkdistance = eventsarray[i].distance;
  //   var checkid = eventsarray[i].id;
  //   var ignore = false;
  //   if(checktime == timestamp){
  //     if(checkdistance == 0){
  //     }
  //      else if(checkdistance < distance){
  //       distance = checkdistance;
  //       myids = [];
  //       mytimes = [];
  //       mydistances = [];
  //       myids.push(checkid);
  //       mytimes.push(checktime);
  //       mydistances.push(checkdistance);
  //     } else if (checkdistance == distance) {
  //       myids.push(checkid);
  //       mytimes.push(checktime);
  //       mydistances.push(checkdistance);
  //     }
  //   } else{
  //     if(timestamp != ''){
  //       allids = allids.concat(myids);
  //       alltimes = alltimes.concat(mytimes);
  //       alldistances = alldistances.concat(mydistances);
  //       timestamp = eventsarray[i].time;
  //       distance = eventsarray[i].distance;
  //       myids = [];
  //       mytimes = [];
  //       mydistances = [];
  //       myids.push(checkid);
  //       mytimes.push(checktime);
  //       mydistances.push(checkdistance);
  //     } else {
  //       timestamp = checktime;
  //       distance = checkdistance;
  //       myids.push(checkid);
  //       mytimes.push(checktime);
  //       mydistances.push(checkdistance);
  //     }
  //   }
  // }
  // allids = allids.concat(myids);
  // alltimes = alltimes.concat(mytimes);
  // alldistances = alldistances.concat(mydistances);
  displayLocations();
}

function displayLocations(){
  var displaytime = alltimes[iterator];
  var myx = 0;
  var myy = 0;
  var myid = allids[i];
  document.getElementById("draw").innerHTML = '';
  var canvas = document.getElementById("draw");
  var ctx = canvas.getContext("2d");
  var img=document.getElementById("floorplanimage");
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.drawImage(img,10,10);
  ctx.strokeStyle="#FF0000";
  document.getElementById("thedate").innerHTML = displaytime;
  for(i = 0; i <alltimes.length; i++){
    if(alltimes[i] == displaytime){
      var myradius = getRadius(alldistances[i]);
      var myx = getX(allids[i]);
      var myy = getY(allids[i]);
      ctx.beginPath();
      ctx.arc(myx,myy,myradius,0,2*Math.PI);
      ctx.stroke();
    }
  }
}

function getRadius(dist){
  if(dist == 0){
    return 0;
  }
  if(dist == 1){
    return 10;
  }
  if(dist == 2){
    return 20;
  }
  if(dist == 3){
    return 40;
  }
}

function getX(myid){
  for(j = 0; j < floorplanbeacons.length; j++){
    if(floorplanbeacons[j] == myid){
      return xcoors[j];
    }
  }
}

function getY(myid){
  for(k = 0; k < floorplanbeacons.length; k++){
    if(floorplanbeacons[k] == myid){
      return ycoors[k];
    }
  }
}

function prev(){
  var checkstamp = alltimes[iterator];
  for(t=iterator; t>=0; t--){
    if(alltimes[t] != checkstamp){
      iterator = t;
      displayLocations();
      break;
    } else if(t == 0){
      alert("There are no previous data points.  Please do a new query to look at earlier information");
    }
  }
}

function next(){
  var checkstamp = alltimes[iterator];
  for(t=iterator; t<alltimes.length; t++){
    if(alltimes[t] != checkstamp){
      iterator = t;
      displayLocations();
      break;
    } else if(t == alltimes.length -1){
      alert("There are no later data points.  Please do a new query to look at later information");
    }
  }
}

function getfloornames(){
  var test = '';
	var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
	xmlHttp.open("POST", "floorplans.php", true); // Starting a POST request (NEVER send passwords as GET variables!!!)
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
	xmlHttp.addEventListener("load", function(event){
		var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
		imageURL = jsonData.image;
    floorName = jsonData.floorplan;
	}, false); // Bind the callback to the load event
	xmlHttp.send(test); // Send the data
}

function byuser(){
  document.getElementById("searchuserdiv").style.display='inline-block';
  document.getElementById("searchbyroomdiv").style.display='none';
  document.getElementById("roomtablespace").innerHTML='';
  document.getElementById("userandtime").style.display='inline-block';
  document.getElementById("roomtablespace").style.display='none';

}
function byroom(){
  document.getElementById("searchbyroomdiv").style.display='inline-block';
  document.getElementById("searchuserdiv").style.display='none';
  document.getElementById("userandtime").style.display='none';
  document.getElementById("roomtablespace").style.display='inline-block';
}

function getfloornames(){
  var test = '';
	var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
	xmlHttp.open("POST", "floorplans.php", true); // Starting a POST request (NEVER send passwords as GET variables!!!)
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
	xmlHttp.addEventListener("load", function(event){
		var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
    listfloornames(jsonData.results);
	}, false); // Bind the callback to the load event
	xmlHttp.send(test); // Send the data
}

function listfloornames(eventsarray){
	document.getElementById("floorplans").innerHTML = "";
	for (var j = 0; j < eventsarray.length; j++){
		var name = eventsarray[j];
    var myselect = '<option value="' + name + '">' + name + '</option>';
    document.getElementById("floorplans").innerHTML += myselect;
	}
}

function selectfloorplan(){
  var myplan = document.getElementById("floorplans").value;
  var myurl = "http://ec2-34-210-27-148.us-west-2.compute.amazonaws.com/~pdhaese/locator/getimage.php?floorname=" + encodeURIComponent(myplan);
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": myurl,
    //"url": "http://ec2-34-210-27-148.us-west-2.compute.amazonaws.com/~pdhaese/locator/retrievepersonlocation.php?partid=will&timestamp1=2017-11-15%2000%3A48%3A53&timestamp2=2017-11-15%2000%3A48%3A55",
    "method": "GET",
    "headers": {
      "cache-control": "no-cache",
      "postman-token": "c00090e1-573e-6c16-2e98-e3d359cb37be"
    }
  }
  $.ajax(settings).done(function (response) {
    var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
    if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData
      displayimage(jsonData.results);
    }
  });
}

function setbeacons(){
  var myplan = document.getElementById("floorplans").value;
  var myurl = "http://ec2-34-210-27-148.us-west-2.compute.amazonaws.com/~pdhaese/locator/getbeacons.php?floorplan=" + encodeURIComponent(myplan);
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": myurl,
    "method": "GET",
    "headers": {
      "cache-control": "no-cache",
      "postman-token": "c00090e1-573e-6c16-2e98-e3d359cb37be"
    }
  }
  $.ajax(settings).done(function (response) {
    var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
    if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData
      parsebeacons(jsonData.results);
    }
  });
}

function parsebeacons(eventsarray){
  floorplanbeacons = [];
  xcoors = [];
  ycoors = [];
  rooms = [];
  for(i = 0; i <eventsarray.length; i++){
    floorplanbeacons.push(eventsarray[i].id);
    xcoors.push(eventsarray[i].x);
    ycoors.push(eventsarray[i].y);
    rooms.push(eventsarray[i].room);
  }
}

function displayimage(myimage){
  document.getElementById("draw").innerHTML = '';
  var img=document.getElementById("floorplanimage");
  img.src = "http://ec2-34-210-27-148.us-west-2.compute.amazonaws.com/~pdhaese/locator/uploads/" + encodeURIComponent(myimage);
  var c=document.getElementById("draw");
  var ctx=c.getContext("2d");
  ctx.drawImage(img,10,10);
}

getfloornames();

document.getElementById("search_by_user").addEventListener("click", searchByUser, false);
document.getElementById("search_by_room").addEventListener("click", searchByRoom, false);
document.getElementById("prev").addEventListener("click", prev, false);
document.getElementById("next").addEventListener("click", next, false);

document.getElementById('byuser').addEventListener("click", byuser, false);
document.getElementById('byroom').addEventListener("click", byroom, false);
document.getElementById('selectfloorplan').addEventListener("click", selectfloorplan, false);
document.getElementById('selectfloorplan').addEventListener("click", setbeacons, false);



// var c=document.getElementById("draw");
// var ctx=c.getContext("2d");
// var img=document.getElementById("floorplanimage");
// ctx.drawImage(img,10,10);
