<?php
require 'database.php';
$floorplan = $_POST['image'];
$organization = $_POST['organization'];
$stmt = $mysqli->prepare("insert into floorplan (image,organization) values (?, ?)");
$stmt->bind_param('ss', $floorplan,$organization);
if(!$stmt){
  $txt = $mysqli->error;
  $txt = "\n";
  exit;
}
$stmt->execute();

$stmt->close();
?>
