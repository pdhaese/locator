# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
This is an indoor positioning system.  This system uses the bluetooth of a mobile device and
estimote beacons to track positions of individuals.  This app links to a database that links to a
front-end that allows users to search for users locations in certain times and search for all users
who have been to a room within a specific time.  The front end also allows the users to upload their
own floorplans for them to set up their own tracking systems.  This system was build using Estimote
Beacons, swift, php, javascript, and mysql.


pdhaese@wustl.edu