//
//  RegisterPage.swift
//  EstimoteTesterApp
//
//  Created by Will Lyons on 10/3/17.
//  Copyright © 2017 Will Lyons. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import MapKit

class RegisterPage: UIViewController{
    @IBOutlet weak var textBox: UITextField!
    override func viewDidAppear(_ animated: Bool){
        
        let test = UserDefaults.standard.bool(forKey: "hasId")
        let toPrint = UserDefaults.standard.dictionaryRepresentation()
        print(test)
        print(toPrint)
        print("HERERE")
        if(test == true){
            print("HELLO")
            self.performSegue(withIdentifier: "bigSeg", sender: self)
        }
    }

    
    
    @IBAction func switchPages(_ sender: Any) {
        print(textBox.text!)
        UserDefaults.standard.set(textBox.text!, forKey: "partId")
        UserDefaults.standard.set("true", forKey: "hasId")
        
        self.performSegue(withIdentifier: "bigSeg", sender: self)
    }
    
}
