//
//  ViewController.swift
//  EstimoteTesterApp
//
//  Created by Will Lyons on 6/7/17.
//  Copyright © 2017 Will Lyons. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class ViewController: UIViewController, ESTBeaconManagerDelegate{
    
    
    lazy var locationManager: CLLocationManager = {
        let manager = CLLocationManager()
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.delegate = self
        manager.pausesLocationUpdatesAutomatically = false
        manager.allowsBackgroundLocationUpdates = true
        //manager.requestWhenInUseAuthorization()
        //manager.requestAlwaysAuthorization()
        return manager
    }()
    fileprivate var locations = [MKPointAnnotation]()
    let defaults = UserDefaults.standard
    let partId = UserDefaults.standard.value(forKey: "partId")
    var fullData = Array<Array<Any>>()
    let beaconManager = ESTBeaconManager()
    let beaconRegion1 = CLBeaconRegion(
        proximityUUID: UUID(uuidString: "C8058239-0A99-4B45-8F1D-819C2FECEC85")!,
        identifier: "ranged region0")
    let beaconRegion2 = CLBeaconRegion(
        proximityUUID: UUID(uuidString: "68105E60-696E-4191-AA54-6901A6F651C4")!,
        identifier: "ranged region1")
    var showMeVar: [CLBeacon] = []
    var showMeBool = false
    var switchBool = false
    var whichRegion = ""
    @IBOutlet weak var helloWorldL: UILabel!
    @IBOutlet weak var testLabel: UILabel!
    @IBOutlet weak var RegionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.startUpdatingLocation()
        self.beaconManager.delegate = self
        self.beaconManager.requestAlwaysAuthorization()
        print(defaults.dictionaryRepresentation())
        // Do any additional setup after loading the view, typically from a nib.
    }
    func beaconManager(_ manager: Any, didEnter region: CLBeaconRegion) {
        print("maybe suppose to be here")
        let proxUUID = String(describing: region.proximityUUID)
        if(proxUUID == "C8058239-0A99-4B45-8F1D-819C2FECEC85"){
            whichRegion = "Region1"
        }
        if(proxUUID == "68105E60-696E-4191-AA54-6901A6F651C4"){
            whichRegion = "Region2"
        }
    }
    @IBAction func stopCollecting(_ sender: Any) {
        print("stop")
        showMeBool = false
        testLabel.text = "OFF"
        switchBool = false
        testLabel.backgroundColor = .red
        locationManager.stopUpdatingLocation()
    }
    
    @IBAction func startCollecting(_ sender: Any) {
        print("start")
        showMeBool = true
        testLabel.text = "ON"
        testLabel.backgroundColor = .green
        locationManager.startUpdatingLocation()
        self.beaconManager.startRangingBeacons(in: self.beaconRegion1)
        self.beaconManager.startRangingBeacons(in: self.beaconRegion2)
    }
    @IBAction func testAPIConnect(_ sender: Any) {
        sendData()
    }
    func sendData(){
        let myUrl = URL(string: "http://ec2-34-210-27-148.us-west-2.compute.amazonaws.com:80/~pdhaese/locator/populatehits.php")!
        var request:URLRequest = URLRequest(url:myUrl)
        let bodyData = "data="+String(describing: fullData)
        request.httpMethod = "POST"
        request.httpBody = bodyData.data(using: String.Encoding.utf8);
        let task = URLSession.shared.dataTask(with: request){
            data, response, error in
            if error != nil{
                print("error is \(String(describing: error))")
            }
            do{
                print(response!)
                if (response?.description.contains("status code: 200"))!{
                    self.switchBool = true
                }
            }
        }
        
        task.resume()
        print("button pressed")
        fullData.removeAll()
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.beaconManager.startRangingBeacons(in: self.beaconRegion1)
        //self.beaconManager.startRangingBeacons(in: self.beaconRegion2)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        //self.beaconManager.stopRangingBeacons(in: self.beaconRegion1)
        //self.beaconManager.startRangingBeacons(in: self.beaconRegion2)
    }


}
extension ViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //print(showMeBool)
        //print("here")
        if(showMeBool == true){
            //print("here")
            self.beaconManager.startRangingBeacons(in: self.beaconRegion1)
            self.beaconManager.startRangingBeacons(in: self.beaconRegion2)
        }
    }
    func beaconManager(_ manager: Any, didRangeBeacons beacons: [CLBeacon],
                       in region: CLBeaconRegion) {
        //print("in beacon manager")
        if(showMeBool == false){
            locationManager.stopUpdatingLocation()
            self.beaconManager.stopRangingBeacons(in: self.beaconRegion1)
            self.beaconManager.stopRangingBeacons(in: self.beaconRegion2)
        }else{
            let date = String(describing: Date())
            let datetime = date.replacingOccurrences(of: " +0000", with: "")
            var temp = [String]()
            //print("SWITCHBOOL:")
            //print(switchBool)
            if(switchBool == true){
                testLabel.text = "success"
            }
            
            if(showMeVar.count > 0){
                for el in showMeVar{
                    let major = String(describing: el.major)
                    let minor = String(describing: el.minor)
                    let prox = String(el.proximity.hashValue)
                    let newLine = [datetime, major, minor, prox, partId as! String]
                    fullData.append(newLine)
                    temp.append(datetime)
                    temp.append(major)
                    temp.append(minor)
                    temp.append(prox)
                    temp.append(partId as! String)
                    
                }
            }
            print(temp)
            print(fullData.count)
            defaults.set(fullData.description, forKey: "check")
            defaults.set(true, forKey: "userDefaultsEdited")
            if(fullData.count > 500){
                sendData()
                fullData.removeAll()
            }
            defaults.synchronize()
            showMeVar = beacons
            helloWorldL.text = "In Beacon Manager"
        }
    }
}

