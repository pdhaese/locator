<?php
require 'database.php';

$eventarray = array();
//SELECT * FROM hits WHERE timestamp >= '2017-11-15 00:48:55' AND timestamp <= '2017-11-15 00:48:56'
$stmt = $mysqli->prepare("SELECT floorname FROM floorplan");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}
$stmt->execute();

$stmt->bind_result($floorname);

while($stmt->fetch()){
	array_push($eventarray,$floorname);
}

$stmt->close();
echo json_encode(array(
	"success" => true,
	"results" => $eventarray
));
exit;
?>
