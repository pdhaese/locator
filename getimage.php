<?php
require 'database.php';

$floorname = $_GET['floorname'];
//SELECT * FROM hits WHERE timestamp >= '2017-11-15 00:48:55' AND timestamp <= '2017-11-15 00:48:56'
$stmt = $mysqli->prepare("SELECT image FROM floorplan  WHERE floorname=?");
$stmt->bind_param('s',$floorname);
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}
$stmt->execute();

$stmt->bind_result($image);
$stmt->fetch();

$stmt->close();
echo json_encode(array(
	"success" => true,
	"results" => $image
));
exit;
?>
