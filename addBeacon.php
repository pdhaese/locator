<?php
require 'database.php';
$id = $_GET['id'];
$x = $_GET['x'];
$y = $_GET['y'];
$floorplan = $_GET['floorplan'];
$room = $_GET['room'];
$stmt = $mysqli->prepare("insert into beacons (id, x,y,floorplan,room) values (?, ?, ?, ?,?)");
$stmt->bind_param('sssss', $id,$x,$y, $floorplan,$room);
if(!$stmt){
  $txt = $mysqli->error;
  $txt = "\n";
  exit;
}
$stmt->execute();

$stmt->close();
?>
